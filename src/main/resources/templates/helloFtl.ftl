<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org"
      xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity3">
<head>
    <title>Hello World!</title>
</head>
<body>
<h1>Hello.v.2</h1>
<p>总数：${res.count}当前显示前2个</p>
<p>We have these animals:
<table border=1>
    <tr>
        <th>test_id</th>
        <th>test_value</th>
    </tr>
    <#list res.data as data>
        <tr>
            <td>${data.test_id}</td>
            <td>${data.test_value}</td>
        </tr>
    </#list>
</table>
</body>
</html>