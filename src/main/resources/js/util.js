/**
 * 获取服务器时间
 */
function getTime(){
    $.ajax({
        type:"POST",
        url:"/user/serviceTime.do",
        success : function(time){
            timer(updateTime, +time);
        }
    });
}
/**
 * 客户端控制时间增加
 * @param updateTime   控制时间格式及输出的函数
 * @param startTime	   起始描述
 * @author zhaohuihbwj
 */
function timer(updateTime, startTime) {
    updateTime(new Date(startTime));
    setInterval(function() {
        var now = new Date(startTime+=1000);
        updateTime(now);
    },1000);
}

//对获得的时间格式进行调整
function  updateTime(now){
    var week = "日一二三四五六";
    var y = now.getFullYear();
    var m = now.getMonth() + 1;
    var d = now.getDate();
    var w = now.getDay();
    var hh = now.getHours();
    var mm = now.getMinutes();
    var ss = now.getSeconds();
    if (ss < 10)
        ss = "0" + ss;
    if (mm < 10)
        mm = "0" + mm;
    var str = y + "年" + m + "月" + d + "日 <font style='font-size:14px;'>" + hh + ":" + mm + ":" + ss + "</font>";

    $("#currentTime").html(str);
}