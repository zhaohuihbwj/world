package nom.zh.world.controller;

import nom.zh.world.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.Map;

/**
 * 测试
 */
@Controller
public class TestController {

    @Autowired
    private TestService testService;


    /**
     * freemarker模板
     * 返回html模板.
     */
    @RequestMapping("/helloFtl")
    public String HelloWorld(ModelMap map) throws Exception {
        map.addAttribute("res", testService.queryAll());
        return "/helloFtl";
    }

    /**
     * thymeleaf模板
     * 返回html模板.
     */
    @RequestMapping("/helloHtml")
    public String helloHtml(Map<String, Object> map) {
        map.put("hello", "from TestController.helloHtml");
        return "/helloHtml";
    }

    /**
     * 获取服务器时间
     * @return time
     */
    @PostMapping("/serviceTime.do")
    public long serviceTime(){
        return System.currentTimeMillis();
    }

    @RequestMapping("/")
    public String index() {
        return "index";
    }

    @RequestMapping("/hello")
    public String hello() {
        return "hello";
    }

}
