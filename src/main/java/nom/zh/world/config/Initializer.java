package nom.zh.world.config;

import org.springframework.session.web.context.AbstractHttpSessionApplicationInitializer;

/**
 * 主要用于向应用容器添加springSessionRepositoryFilter,顺便注册一下HttpSessionEventPublisher监听,这个监听的作用发布HttpSessionCreatedEvent和HttpSessionDestroyedEvent事件
 * @author zhaohuihbwj
 */
public class Initializer extends AbstractHttpSessionApplicationInitializer {

}
