package nom.zh.world.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * 来启用RedisHttpSession,在这个配置注册一个redis客户端的连接工厂Bean,供Spring Session用于与redis服务端交互.
 * @author zhaohuihbwj
 */
@Configuration
@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 10*60)
public class RedisHttpSessionConfig {

    /**
     * 配置的这个Bean，是让Spring根据配置文件中的配置连到Redis。
     * @return
     */
    @Bean
    public JedisConnectionFactory connectionFactory() {
        return new JedisConnectionFactory();
    }

}
