package nom.zh.world.dao;

import nom.zh.world.dto.UserInfo;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {

    /**
     * 根据用户名获取用户
     *
     * @param userName
     * @return
     */
    UserInfo getUserByName(@Param("userName") String userName);
}
