package nom.zh.world.dao;

import java.util.List;
import java.util.Map;

/**
 * 测试
 */
public interface TestMapper {

    List<Map<String, Object>> queryAll() throws Exception;
}
