package nom.zh.world.dao;

import nom.zh.world.dto.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleMapper {

    /**
     * 根据用户ID获取用户角色
     *
     * @param userId
     * @return
     */
    List<Role> getUserRole(@Param("userId") Integer userId);
}
