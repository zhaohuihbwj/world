package nom.zh.world.service;

import java.util.Map;

/**
 * 测试
 */
public interface TestService {

    Map<String, Object> queryAll() throws Exception;
}
