package nom.zh.world.service;

import nom.zh.world.dto.UserInfo;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    /**
     * 根据用户名获取用户
     *
     * @param userName
     * @return
     */
    UserInfo getUserByName(String userName);
}
