package nom.zh.world.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import nom.zh.world.dao.TestMapper;
import nom.zh.world.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TestServiceImpl implements TestService {

    @Autowired
    private TestMapper testMapper;

    @Override
    public Map<String, Object> queryAll() throws Exception {
        //返回值
        Map<String, Object> result = new HashMap<>();

        //分页
        PageHelper.startPage(1, 2);
        List<Map<String, Object>> mapList = testMapper.queryAll();

        //总数
        PageInfo<Map<String, Object>> page = new PageInfo<>(mapList);
        long count = page.getTotal();

        result.put("data", mapList);
        result.put("count", count);

        return result;
    }
}
