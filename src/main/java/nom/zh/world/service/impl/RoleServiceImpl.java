package nom.zh.world.service.impl;

import nom.zh.world.dao.RoleMapper;
import nom.zh.world.dto.Role;
import nom.zh.world.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public List<Role> getUserRole(Integer userId) {
        List<Role> roles = roleMapper.getUserRole(userId);
        return roles;
    }
}
