package nom.zh.world.service;

import nom.zh.world.dto.Role;

import java.util.List;

public interface RoleService {

    /**
     * 根据用户ID获取用户角色
     *
     * @param userId
     * @return
     */
    List<Role> getUserRole(Integer userId);
}
